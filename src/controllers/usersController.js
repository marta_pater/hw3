const express = require('express');
const router = express.Router();

const {
    getUserByUserId,
    deleteUserByUserId,
    changePasswordForUser
} = require('../services/usersService');

router.get('/me', async (req, res) => {
    const { userId } = req.user;

    const user = await getUserByUserId(userId);

    res.json({user});
});

router.delete('/me', async (req, res) => {
    const { userId } = req.user;

    await deleteUserByUserId(userId);

    res.json({message: "Profile deleted successfully"});
});

router.patch('/me/password', async (req, res) => {
    const { userId } = req.user;
    const { oldPassword } = req.body.oldPassword;
    const { newPassword } = req.body.newPassword;

    await changePasswordForUser(userId, oldPassword, newPassword);

    res.json({message: "Password changed successfully"});
});

module.exports = {
    usersRouter: router
}