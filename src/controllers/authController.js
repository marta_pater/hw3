const express = require('express');
const router = express.Router();

const {
    registration,
    login,
    forgotPassword
} = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            email,
            password,
            role
        } = req.body;

        await registration({email, password, role});

        res.json({message: 'Profile created successfully'});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/login', async (req, res) => {
    try {
        const {
            email,
            password
        } = req.body;

        const jwt_token = await login({email, password});

        res.json({jwt_token});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/forgot_password', async (req, res) => {
    try {
        const email = req.body;

        await forgotPassword(email);

        res.json({message: 'New password sent to your email address'});
    } catch (err) {
        res.status(500).json({message: err.message});
    }
});

module.exports = {
    authRouter: router
}