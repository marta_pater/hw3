const express = require('express');
const router = express.Router();

const {
    getTrucksByUserId,
    addTruckToUser,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser
} = require('../services/trucksService');

router.get('/', async (req, res) => {
    const { userId } = req.user;

    const trucks = await getTrucksByUserId(userId);

    res.status(200).json({trucks});
});

router.get('/:id', async (req, res) => {
    try{
        const { userId } = req.user;
        const { id } = req.params;

        const truck = await getTruckByIdForUser(id, userId);

        if (!truck) {
            throw new Error('No truck with such id found!');
        }

        res.status(200).json({truck});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.post('/', async (req, res) => {
    try{
        const { userId } = req.user;

        await addTruckToUser(userId, req.body);

        res.status(200).json({message: "Truck created successfully"});
    } catch {
        res.status(500).json({message: err.message});
    }
});

router.put('/:id', async (req, res) => {
    try{
        const { userId } = req.user;
        const { id } = req.params;
        const data = req.body.type;

        await updateTruckByIdForUser(id, userId, data);

        res.status(200).json({message: "Truck details changed successfully"});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

router.delete('/:id', async (req, res) => {
    try{
        const { userId } = req.user;
        const { id } = req.params;

        await deleteTruckByIdForUser(id, userId);

        res.status(200).json({message: "Truck deleted successfully"});
    } catch(err) {
        res.status(500).json({message: err.message});
    }
});

module.exports = {
    trucksRouter: router
}