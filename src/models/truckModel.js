const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    created_by: {
        type: mongoose.Schema.Types.ObjectId
    },

    assigned_to: {
        type: mongoose.Schema.Types.ObjectId
    },

    type: {
        type: String,
        required: true
    },

    status: {
        type: String,
        default: "IS"
    },

    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Truck };