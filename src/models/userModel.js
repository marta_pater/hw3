const mongoose = require('mongoose');

const User = mongoose.model('User', {
    email: {
        type: String,
        required: true,
        unique: true
    },
    
    password: {
        type: String,
        required: true
    },

    role: {
        type: String,
        required: true,
        uppercase: true
    },

    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { User };