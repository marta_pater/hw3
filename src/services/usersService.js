const {User} = require('../models/userModel');

const getUserByUserId = async (userId) => {
    const user = await User.findById({_id: userId});

    if (user) {
        user.password = undefined;
        user.__v = undefined;
    } else {
        throw new Error('No user with such id found!');
    }

    return user;
}

const deleteUserByUserId = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

const changePasswordForUser = async (userId, newPassword) => {
    await User.findOneAndUpdate({_id: userId}, {password: await bcrypt.hash(newPassword, 10)});
}

module.exports = {
    getUserByUserId,
    deleteUserByUserId,
    changePasswordForUser
};