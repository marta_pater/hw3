const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({email, password, role}) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role
    });
    await user.save();
}

const login = async ({email, password}) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('string');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('string');
    }

    const jwt_token = jwt.sign({
        _id: user._id,
        email: user.email
    }, 'secret');
    return jwt_token;
}

const forgotPassword = async(email) => {
    await User.findOne({email});
}

module.exports = {
    registration,
    login,
    forgotPassword
};